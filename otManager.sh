#!/bin/bash

tasks=$1
servers=$2
# pemKey=$3

echo "
pipeline {
    agent any
    stages {
         stage('Checkout Code') {
            steps {
                git 'https://gitlab.com/yamank.gupta/workshop-jenkins.git'
            }
        }
" >downstreamPipeline.groovy

echo -n "" >script.sh
echo -e "#!/bin/bash" >>script.sh
echo "source ./modules.sh" >>script.sh
echo -n "" >report.txt
while read task; do
    server=$(echo "$task" | awk -F',' '{print $1}')
    operation=$(echo "$task" | awk -F',' '{print $2}')
    entity=$(echo "$task" | awk -F',' '{print $3}')

    if grep -q "^$server" $servers; then
        # echo -e "\nInitiating Connection request to $server"
        user=$(grep "^$server" $servers | awk -F',' '{print $2}')
        ip=$(grep "^$server" $servers | awk -F',' '{print $3}')
        pemKey=$(grep "^$server" $servers | awk -F',' '{print $4}')
        # chmod 400 $pemKey
        if [[ $operation == "fileWithContent" ]]; then
            content=$(echo "$task" | awk -F',' '{print $4}')
        else
            echo -n ""
        fi
        command='ls'

        case $operation in

        "install")
            command="sudo apt install $entity -y"
            ;;
        "file")
            command="touch $entity"
            ;;
        "directory")
            command="mkdir $entity"
            ;;
        'user')
            command="sudo useradd $entity"
            ;;
        "group")
            command="groupadd $entity"
            ;;
        "fileWithContent")
            command="echo \"$content\" > $entity "
            ;;
        *)
            echo "hi"
            ;;
        esac
        echo " echo -e \"\nWorking in $server.... \"" >>script.sh
        echo "ssh -i $pemKey $user@$ip \"$command\"" >>script.sh
        echo "printMessage \$? $operation $entity $server" >>script.sh        
        echo " stage('$server-$operation-$entity') {
            steps {
              sh \"ssh -i $pemKey $user@$ip '$command'\"  
            }
        }
        " >>downstreamPipeline.groovy


    else

        echo "Sorry, $server is nt known to us"
    fi

done <$tasks

chmod 777 script.sh
echo "
    }
}    ">>downstreamPipeline.groovy
echo "echo -e \" \n\n\n ||| F-I-N-A-L  R-E-P-O-R-T |||\n\n\" " >>script.sh
echo "cat report.txt" >>script.sh
echo "rm report.txt" >>script.sh
./script.sh
rm script.sh
# git add .
# git commit -m "auto commit"
# git push origin master