pipeline {
    agent any
    stages {
        stage('Checkout Code') {
            steps {
      git 'https://gitlab.com/yamank.gupta/workshop-jenkins.git'
            }
        }
        stage('Initialising Downstream Tasks Pipeline') {
            steps {
              "sh otManager.sh"
            }
        }
        
       stage('Invoking Downstream pipeline') {
            steps {
               script{
                   build job : 'downstreamPipeline'
               }
            }
       }
        
    }
}